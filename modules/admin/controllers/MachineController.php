<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use app\models\Machine;
use app\models\Category;
use app\modules\admin\controllers\traits\BehaviorsTrait;

/**
 * Default controller for the `admin` module
 */
class MachineController extends Controller
{
    use BehaviorsTrait;
    
    public function actionIndex()
    {
        $machines = Machine::getMachinesFromWialon();          
        return $this->render('index', [
            'machines' => $machines,
        ]);
    }
    
    public function actionUpdate()
    {
        $model = Machine::getCurrent();
                
        if ($model->load(Yii::$app->request->post()) && $model->uploadImages() && $model->save()) {
            $model->saveInfo();
            Yii::$app->session->setFlash('success', "Информация изменена");
        }          
        return $this->render('update', [
            'model' => $model,
        ]);
    } 
    
    public function actionCreate()
    {
        $post = Yii::$app->request->post();
        $machine = new Machine();
        $machine->label = $post['wialon_name'];
        $machine->wialon_name = $post['wialon_name']; 
        $machine->wialon_id = $post['wialon_id'];
        $machine->stats_update = time();
        $sampleModel = Machine::findOne(1);
        $machine->sensors = $sampleModel->sensors;
        $machine->sensors->time = time();
        $machine->save();        
        foreach($sampleModel->categories as $category) {
            if(!$category->parent_id) {
                $newCategoryId = $category->cloneSelf($machine->id);
                foreach($category->categories as $subcategory) {
                    $subcategory->cloneSelf($machine->id, $newCategoryId);
                }
            }
        }
        return $this->redirect(['/admin/machine/index']);
    }
    
    public function actionChoose()
    {
        $post = Yii::$app->request->post();
        Machine::choose($post['machineId']);
        return $this->redirect(Yii::$app->request->referrer);
    }
}
