<?php

namespace app\modules\admin\controllers\traits;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\User;

trait BehaviorsTrait {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->role == User::ROLE_ADMIN;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'add-machine' => ['post'],
                ],
            ],
        ];
    }

}
