<?php

namespace app\modules\admin;

use Yii;
use yii\base\BootstrapInterface;
use app\models\Machine;


class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        if(!isset(Yii::$app->request->cookies['machineId'])) {
            Machine::chooseDefault();
        }
    }
}