<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\AR\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Пользователь</h3>
        </div>

        <div class="box-body">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?php if (!$model->password_hash): ?>
                <?= $form->field($model, 'password', [
                    'template' => '
                        {label}
                        <div class="input-group password-input">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default btn-flat generate" aria-label="Случайный пароль" title="Случайный пароль">
                                    <i class="fa fa-random"></i>
                                </button>
                            </div>
                            {input}
                        </div>
                        {error}{hint}',
                ])->passwordInput(['maxlength' => true])->hint('-', ['class' => 'password-hint']) ?>
            <?php endif; ?>

            <?= $form->field($model, 'status')->dropDownList(User::getStatuses()) ?>

            <?= $form->field($model, 'role')->dropDownList(User::getRoles(), ['id' => 'user-role-input']) ?>
            <div id="machine-id-block">
                <?= $form->field($model, 'machine_id')->dropDownList(\app\models\Machine::getMachineList()) ?>
            </div>
        </div>
    </div>
    
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-block']) ?>

    <?php ActiveForm::end(); ?>
</div>
<?php
$admin = User::ROLE_ADMIN;
$script = <<< JS
$(document).ready(function() {
    if($("#user-role-input").val() == '{$admin}') {
        $("#machine-id-block").addClass('hidden');
    }
});
        
$("#user-role-input").change(function(){
   if(this.value == '{$admin}') {
        $("#machine-id-block").addClass('hidden');
    } else {
        $("#machine-id-block").removeClass('hidden');
    };   
});
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>