<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Feature */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feature-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
            'data' => Category::getModelsList(($model->machine_id) ?: 1),
        ])->hint('Поиск по названию') ?>
    
    <?= $form->field($model, 'isComposite')->checkbox() ?>
    
    <?= $form->field($model, 'is_calc')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
