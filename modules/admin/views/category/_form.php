<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Machine;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
$machines = Machine::getMachineList();
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'parent_id')->widget(Select2::classname(), [
            'data' => $model->getParentsList($model->id),
            'options' => [
                'prompt' => '- Верхний уровень -',
            ],
        ])->hint('Поиск по названию') ?>
    
        <?= $form->field($model, 'machine_id')->widget(Select2::classname(), [
            'data' => $machines,
            'options' => ($model->machine_id) ? [] : [
                'prompt' => '- Все -',
            ],
        ])->hint('Поиск по названию') ?>

        <?= $form->field($model, 'complexFeature')->checkbox() ?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
