<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Parameter;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Machine */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-default-index">
    <div class="machine-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ($model->categories as $category) : ?>
                <?php if (!$category->parent_id) : ?>
                    <li class="<?= ($category->name == "rig") ? "active" : "" ?>">
                        <a href="#category-<?= $category->id ?>" aria-controls="home" role="tab" data-toggle="tab"><?= $category->label; ?></a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li>
                <a href="#category-images" aria-controls="home" role="tab" data-toggle="tab">Изображения</a>
            </li>
        </ul>
        <br>
        <div class="tab-content">
            <?php foreach ($model->categories as $category) : ?>
                <?php if (!$category->parent_id) : ?>
                    <div role="tabpanel" class="tab-pane <?= ($category->name == "rig") ? "active" : "" ?>" id="category-<?= $category->id ?>">
                        <div class="panel panel-success">
                            <div class="panel-heading"><?= $category->label; ?></div>
                            <div class="panel-body">
                                <div class="row categoryFeatures row-flex">
                                    <?php if ($category->getFeatureValues()) : ?>
                                        <?=
                                        $this->render('_formSimple', [
                                            'model' => $model,
                                            'category' => $category,
                                            'form' => $form,
                                        ])
                                        ?>
                                    <?php endif; ?>
                                    <?php foreach ($category->categories as $subcategory) : ?>
                                        <div class="panel panel-success panel-subcategory">
                                            <div class="panel-heading"><?= $subcategory->label; ?></div>
                                            <div class="panel-body">
                                                <div class="row categoryFeatures row-flex">
                                                    <?=
                                                    $this->render(($subcategory->complexFeature) ? '_formComplex' : '_formSimple', [
                                                        'model' => $model,
                                                        'category' => $subcategory,
                                                        'form' => $form,
                                                    ])
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            <div role="tabpanel" class="tab-pane" id="category-images">
                <div class="panel panel-success">
                    <div class="panel-heading"><?= $category->label; ?></div>
                    <div class="panel-body">
                        <div class="row categoryFeatures row-flex">
                            <div class="col-md-6">
                            <?= Html::label($model->attributeLabels()['mainImage'])?>
                            <?= $form->field($model, 'mainImage')->fileInput() ?>
                            <?= Html::img(['/images/' . $model->id . '-mainImage.png'], ['width' => 200]) ?>
                            </div>
                            <div class="col-md-6">
                            <?= Html::label($model->attributeLabels()['schemaImage'])?>
                            <?= $form->field($model, 'schemaImage')->fileInput() ?>
                            <?= Html::img(['/images/' . $model->id . '-schemaImage.png'], ['width' => 200]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>        

    </div>
</div>
