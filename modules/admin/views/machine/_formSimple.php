<?php
    use unclead\multipleinput\MultipleInput;
?>
<?=
$form->field($model, 'info[' . $category->id . ']')->widget(MultipleInput::className(), [
    'data' => $category->getFeatureValues(),
    'columns' => [
        [
            'name' => 'featureId',
            'title' => '',
            'enableError' => true,
            'options' => [
                'class' => 'hidden'
            ]
        ],
        [
            'name' => 'categoryId',
            'title' => '',
            'defaultValue' => $category->id,
            'enableError' => true,
            'options' => [
                'class' => 'hidden'
            ]
        ],
        [
            'name' => 'featureLabel',
            'title' => 'Свойство',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority'
            ]
        ],
        [
            'name' => 'defaultId',
            'title' => '',
            'enableError' => true,
            'options' => [
                'class' => 'hidden'
            ]
        ],
        [
            'name' => 'defaultValue',
            'title' => 'Значение',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority'
            ]
        ],
    ]
]);
?>