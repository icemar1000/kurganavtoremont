<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Parameter;
use unclead\multipleinput\MultipleInput;
?>

<div class="admin-default-machine">
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Название машины в системе Wialon</th>
                <th>Состояние</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($machines as $id => $machine) :?>
            <tr>
                <td><?= $machine['name'] ?></td>
                <td><?= ($machine['isAdd']) ? "Добавлена" : 
                    Html::beginForm(['/admin/machine/create'], 'post')
                    . Html::hiddenInput('wialon_name', $machine['name'])
                    . Html::hiddenInput('wialon_id', $id)
                    . Html::submitButton('Добавить', ['class' => 'btn btn-success'])
                    . Html::endForm()
                    ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
</div>

