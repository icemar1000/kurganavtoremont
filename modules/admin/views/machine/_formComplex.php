<?php

use unclead\multipleinput\MultipleInput;

?>
<?=

$form->field($model, 'info[' . $category->id . ']')->widget(MultipleInput::className(), [
    'data' => $category->getFeatureValues(),
    'columns' => [
        [
            'name' => 'featureId',
            'title' => '',
            'enableError' => true,
            'options' => [
                'class' => 'hidden'
            ]
        ],
        [
            'name' => 'categoryId',
            'title' => '',
            'defaultValue' => $category->id,
            'enableError' => true,
            'options' => [
                'class' => 'hidden'
            ]
        ],
        [
            'name' => 'featureLabel',
            'title' => 'Свойство',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority'
            ]
        ],
        [
            'name' => 'modelId',
            'title' => '',
            'enableError' => true,
            'options' => [
                'class' => 'hidden'
            ]
        ],
        [
            'name' => 'modelValue',
            'title' => 'Марка/Модель',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority'
            ]
        ],
        [
            'name' => 'modelNumberId',
            'title' => '',
            'enableError' => true,
            'options' => [
                'class' => 'hidden'
            ]
        ],
        [
            'name' => 'modelNumberValue',
            'title' => 'Номер',
            'enableError' => true,
            'options' => [
                'class' => 'input-priority'
            ]
        ]
    ]
]);
?>