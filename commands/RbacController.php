<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\rbac\UserRoleRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $permissionViewer = $auth->createPermission('viewer');
        $permissionAdmin = $auth->createPermission('admin');

        $auth->add($permissionViewer);
        $auth->add($permissionAdmin);

        $rule = new UserRoleRule();

        $auth->add($rule);

        $viewer = $auth->createRole('viewer');
        $viewer->ruleName = $rule->name;

        $auth->add($viewer);

        $admin = $auth->createRole('admin');
        $admin->ruleName = $rule->name;

        $auth->add($admin);

        $auth->addChild($admin, $viewer);
    }
}