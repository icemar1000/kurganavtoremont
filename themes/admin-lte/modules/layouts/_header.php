<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\ChooseMachineWidget;

/* @var $this \yii\web\View */
/* @var $content string */

$logo = '<span class="logo-mini">СМ</span>'
        . '<span class="logo-lg">Система_мониторинга</span>';
?>

<header class="main-header">

    <?= Html::a($logo, Yii::$app->homeUrl, [
        'class' => 'logo',
        'target' => '_blank',
    ]) ?>    
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <?= ChooseMachineWidget::widget() ?>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a('Изменить пароль', ['/admin/profile/change-password'], [
                                    'class' => 'btn btn-warning btn-flat',
                                ]) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a('Выйти', ['/site/logout'], [
                                    'data-method' => 'post',
                                    'class' => 'btn btn-default btn-flat',
                                ]) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
