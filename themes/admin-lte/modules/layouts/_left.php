<?php

use dmstr\widgets\Menu;

/* @var $this yii\web\View */

$controller = Yii::$app->controller->id;
$action     = Yii::$app->controller->action->id;

$checkRoute = function ($c, $a = null) use ($controller, $action) {
    $result = $c == $controller;
    if ($a !== null) {
        $result &= $a == $action;
    }
    return $result;
};
?>

<aside class="main-sidebar">
    <section class="sidebar">
        <?= Menu::widget([
            'options' => ['class' => 'sidebar-menu'],
            'items' => [
                [
                    'label'  => 'Замена узлов',
                    'icon'   => 'map',
                    'url'    => ["/admin/machine/update"],
                    'active' => $checkRoute('default', 'node'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Управление машинами',
                    'icon'    => 'car',
                    'url'     => ["/admin/machine/index"],
                    'active'  => $checkRoute('page'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Ремонт',
                    'icon'    => 'map-signs',
                    'url'     => ["#"],
                    'active'  => $checkRoute('node'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Тех.обслуживание',
                    'icon'    => 'file-text-o',
                    'url'     => ["#"],
                    'active'  => $checkRoute('page'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Закрепление',
                    'icon'    => 'bookmark',
                    'url'     => ["#"],
                    'active'  => $checkRoute('tag'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Буровой инструмент',
                    'icon'    => 'cube',
                    'url'     => ["#"],
                    'active'  => $checkRoute('object'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Обновление ПО',
                    'icon'    => 'history',
                    'url'     => ["#"],
                    'active'  => $checkRoute('admin-log'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Пользователи',
                    'icon'    => 'users',
                    'url'     => ["/admin/user"],
                    'active'  => $checkRoute('user'),
                    'visible' => Yii::$app->user->can('admin'),
                ]
            ],
        ]) ?>
    </section>
</aside>
