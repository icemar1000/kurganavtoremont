<?php

namespace app\models;

use Yii;
use app\models\traits\ModelsListTrait;
use app\models\Parameter;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $position
 * @property string $label
 * @property int $parent_id
 * @property int $machine_id
 *
 * @property Machine $machine
 * @property Category $parent
 * @property Category[] $categories
 * @property Feature[] $features
 */
class Category extends \yii\db\ActiveRecord {

    use ModelsListTrait;

    public $featuresForSave = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['position', 'parent_id', 'machine_id'], 'integer'],
                [['label'], 'required'],
                [['label', 'name'], 'string', 'max' => 255],
                [['complexFeature'], 'boolean'],
                [['machine_id'], 'exist', 'skipOnError' => true, 'targetClass' => Machine::className(), 'targetAttribute' => ['machine_id' => 'id']],
                [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'position' => 'Position',
            'label' => 'Название',
            'parent_id' => 'Родительская категория',
            'machine_id' => 'Машина',
            'complexFeature' => 'Содержит свойства с комплексным значением'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMachine() {
        return $this->hasOne(Machine::className(), ['id' => 'machine_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories() {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatures() {
        return $this->hasMany(Feature::className(), ['category_id' => 'id']);
    }

    public function getFeatureValues() {
        $result = [];
        $params = Parameter::getModelsById();
        foreach ($this->features as $key => $feature) {
            $result[$key]['featureId'] = $feature->id;
            $result[$key]['featureLabel'] = $feature->label;
            foreach ($feature->featureValues as $featureValue) {
                $result[$key][$params[$featureValue->parameter_id]['name'] . 'Id'] = $featureValue->id;
                $result[$key][$params[$featureValue->parameter_id]['name'] . 'Value'] = 
                    ($feature->name == 'engine_hours') ? round($featureValue->value / (60*60), 1) : $featureValue->value;
            }
        }

        return $result;
    }

    public function cloneSelf($machineId, $parentId = null) 
    {
        $model = new Category();
        $model->attributes = $this->attributes;
        if($parentId) {
            $model->parent_id = $parentId;
        }
        $model->machine_id = $machineId;
        $model->save();
        foreach($this->features as $feature) {
            $feature->cloneSelf($machineId, $model->id);
        }
        
        return $model->id;
    }

}
