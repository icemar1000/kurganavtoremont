<?php

namespace app\models;

use Yii;
use app\models\FeatureValue;

/**
 * This is the model class for table "feature".
 *
 * @property int $id
 * @property int $position
 * @property string $label
 * @property string $name
 * @property int $category_id
 *
 * @property Category $category
 * @property FeatureValue[] $featureValues
 */
class Feature extends \yii\db\ActiveRecord
{
    
    public $isComposite = false;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feature';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position', 'category_id', 'machine_id'], 'integer'],
            [['label', 'category_id', 'machine_id'], 'required'],
            [['label', 'name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['isComposite', 'is_calc'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'Position',
            'label' => 'Label',
            'name' => 'Name',
            'category_id' => 'Category ID',
            'isComposite' => 'Множественное значение',
            'is_calc' => 'Калькулируемое значение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatureValues()
    {
        return $this->hasMany(FeatureValue::className(), ['feature_id' => 'id']);
    }
    
    public function cloneSelf($machineId, $categoryId)
    {
        $model = new Feature();
        $model->attributes = $this->attributes;
        $model->category_id = $categoryId;
        $model->machine_id = $machineId;
        $model->save();
        foreach($this->featureValues as $featureValue) {
            FeatureValue::create($machineId, $model->id, $featureValue->parameter_id, '0');
        }
        
        return $model->id;
    }
}