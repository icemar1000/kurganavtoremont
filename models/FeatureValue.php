<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feature_value".
 *
 * @property int $id
 * @property int $machine_id
 * @property int $feature_id
 * @property int $parameter_id
 *
 * @property Parameter $parameter
 * @property Feature $feature
 * @property Machine $machine
 */
class FeatureValue extends \yii\db\ActiveRecord {
    
    public $notSetUpdatedAt = false;
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'feature_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['machine_id', 'feature_id', 'parameter_id'], 'required'],
                [['machine_id', 'feature_id', 'parameter_id'], 'integer'],
                [['value'], 'string', 'max' => 255],
                [['parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['parameter_id' => 'id']],
                [['feature_id'], 'exist', 'skipOnError' => true, 'targetClass' => Feature::className(), 'targetAttribute' => ['feature_id' => 'id']],
                [['machine_id'], 'exist', 'skipOnError' => true, 'targetClass' => Machine::className(), 'targetAttribute' => ['machine_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'machine_id' => 'Machine ID',
            'feature_id' => 'Feature ID',
            'parameter_id' => 'Parameter ID',
            'value' => 'Значение'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameter() {
        return $this->hasOne(Parameter::className(), ['id' => 'parameter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeature() {
        return $this->hasOne(Feature::className(), ['id' => 'feature_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMachine() {
        return $this->hasOne(Machine::className(), ['id' => 'machine_id']);
    }

    public static function create($machineId, $featureId, $parameterId, $value) {
        $featureValue = new FeatureValue();
        $featureValue->machine_id = $machineId;
        $featureValue->feature_id = $featureId;
        $featureValue->parameter_id = $parameterId;
        $featureValue->value = $value;
        return $featureValue->save();
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->value = (string) $this->value;
            if(!$this->notSetUpdatedAt) {
                $this->updated_at = time();
            }            
            return true;
        }
        return false;
    }
}
