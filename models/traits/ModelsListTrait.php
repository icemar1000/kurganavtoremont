<?php

namespace app\models\traits;

use yii\helpers\ArrayHelper;
use yii\db\Expression;

trait ModelsListTrait {

    /**
     * @param null|int $machineId
     * @return array
     */
    public static function getModelsList($machineId = null) {
        $query = parent::find()->select(['id', 'label']);
        if($machineId) {
            $query->andWhere(['machine_id' => $machineId]);
        }
        $models = $query->all();
        return ArrayHelper::map($models, 'id', 'label');
    }

    /**
     * @return array
     */
    public static function getModelsById() {
        $models = parent::find()->asArray()->all();
        return ArrayHelper::index($models, 'id');
    }
    
    public static function getModelsByName() {
        $models = parent::find()->asArray()->all();
        return ArrayHelper::index($models, 'name');
    }

    /**
     * @return array
     */
    public static function getModelsWithParentsList() {
        $models = parent::find()->select([
                    'id',
                    'name',
                    'parent_id',
                    new Expression('if(position is null, 1, 0) as position_flag'),
                ])->orderBy([
                    'parent_id' => SORT_ASC,
                    'position_flag' => SORT_ASC,
                    'position' => SORT_ASC,
                ])->all();

        $list = [];

        foreach ($models as $_model) {
            $name = !$_model->parent_id ? $_model->name : $_model->parentName;
            $id = !$_model->parent_id ? $_model->id : $_model->parent_id;
            $prefix = !$_model->parent_id ? '' : '> ';
            $list["{$name} [{$id}]"][$_model->id] = $prefix . $_model->name;
        }

        return $list;
    }

    /**
     * @param array|null $tree
     * @return array
     */
    public static function getModelsTreeList($tree = null) {
        if ($tree === null) {
            $tree = self::getModelsTree();
        }

        $list = [];

        foreach ($tree as $model) {
            $key = "{$model['name']} [{$model['id']}]";
            $list[$key][$model['id']] = $model['name'];

            if (!empty($model['descendants'])) {
                $list[$key] += self::getModelDescendantsList($model);
            }
        }

        return $list;
    }

    /**
     * @param array $model
     */
    public static function getModelDescendantsList($model, $level = 0) {
        $list = [];

        if (isset($model['descendants']) && !empty($model['descendants'])) {
            foreach ($model['descendants'] as $_model) {
                $hasDescendants = isset($_model['descendants']) && !empty($_model['descendants']);

                $prefix = self::getLevelPrefix($level) . ($hasDescendants ? '+' : '>') . ' ';
                $list[$_model['id']] = $prefix . $_model['name'];

                if ($hasDescendants) {
                    $list += self::getModelDescendantsList($_model, $level + 1);
                }
            }
        }

        return $list;
    }

    /**
     * @param int $level
     */
    public static function getLevelPrefix($level) {
        $prefix = '';

        if ($level == 0) {
            return $prefix;
        }

        for ($i = 0; $i < $level; $i++) {
            $prefix .= '-- ';
        }

        return $prefix;
    }

    /**
     * @param int $id
     * @return array
     */
    public static function getParentsList($id = null) {
        $query = parent::find()->select([
                    'id',
                    'label',
                    'parent_id',
                    new Expression('if(position is null, 1, 0) as position_flag'),
                ])->andWhere([
                    'parent_id' => null,
                ])->orderBy([
            'position_flag' => SORT_ASC,
            'position' => SORT_ASC,
        ]);

        if ($id !== null) {
            $query->andWhere(['<>', 'id', $id]);
        }

        $models = $query->all();

        return ArrayHelper::map($models, 'id', 'label');
    }

    /**
     * @param int|null $parent
     * @param int|null $ignore
     * @param bool $onlyActive
     * @return array
     */
    public static function getModelsTree($parent = null, $ignore = null, $onlyActive = false) {
        $preQuery = $onlyActive ? self::findActive() : parent::find();

        $query = $preQuery->select([
                    'id',
                    'name',
                    'parent_id',
                    new Expression('if(position is null, 1, 0) as position_flag'),
                ])->orderBy([
            'position_flag' => SORT_ASC,
            'position' => SORT_ASC,
        ]);

        $_models = $query->asArray()->all();
        $models = [];

        foreach ($_models as $model) {
            $models[$model['id']] = $model;
        }

        $tree = self::makeTree($models, null, $ignore);

        if ($parent) {
            return self::filterTree($tree, $parent);
        }

        return $tree;
    }

    /**
     * @param array $models
     * @param int|null $parent
     * @param int|null $ignore
     * @return array
     */
    public static function makeTree($models = [], $parent = null, $ignore = null) {
        $tree = [];

        foreach ($models as $id => $model) {
            if ($model['parent_id'] == $parent && $id != $model['parent_id']) {
                unset($models[$id]);
                $tree[$id] = [
                    'id' => $model['id'],
                    'name' => $model['name'],
                    'parent_id' => $model['parent_id'],
                    'descendants' => self::makeTree($models, $id),
                ];
                if ($ignore == $id) {
                    unset($tree[$id]);
                }
            }
        }

        return $tree;
    }

    /**
     * @param array $tree
     * @param integer $id
     * @return array
     */
    public static function filterTree($tree, $id) {
        $return = [];

        foreach ($tree as $key => $model) {
            if ($key == $id) {
                return $model;
            }
            if (is_array($model)) {
                $_models = self::filterTree($model, $id);
                if (!empty($_models)) {
                    return $_models;
                }
            }
        }

        return $return;
    }

    /**
     * @param array|null $tree
     * @return array
     */
    public static function getModelsTreeIds($tree = null) {
        if ($tree === null) {
            $tree = self::getModelsTree();
        }

        $ids = [];

        foreach ($tree as $model) {
            if (isset($model['id'])) {
                $ids[] = $model['id'];
            }
            if (isset($model['descendants']) && !empty($model['descendants'])) {
                $ids = array_merge(self::getModelsTreeIds($model['descendants']), $ids);
            }
            if (is_array($model) && !isset($model['descendants'])) {
                $ids = array_merge(self::getModelsTreeIds($model), $ids);
            }
        }

        return $ids;
    }

    /**
     * @param bool $onlyActive
     * @return array
     */
    public function getTree($onlyActive = false) {
        if ($this->isNewRecord) {
            return [];
        }

        return self::getModelsTree($this->id, null, $onlyActive);
    }

    /**
     * @param bool $onlyActive
     * @return array
     */
    public function getTreeIds($onlyActive = false) {
        $descendants = self::getModelsTreeIds($this->getTree($onlyActive));
        return array_merge($descendants, [$this->id]);
    }

    /**
     * @return array
     */
    public function getAvailableParentsTreeList() {
        if ($this->isNewRecord) {
            return self::getModelsTreeList();
        }

        $tree = self::getModelsTree(null, $this->id);
        return self::getModelsTreeList($tree);
    }

    /**
     * @return array
     */
    public function getParentsChain() {
        $chain = [];
        $model = $this;

        while (isset($model->parent) && $model->parent) {
            $model = $model->parent;
            $chain[$model->id] = $model;
        }

        return array_reverse($chain);
    }

    /**
     * @return array
     */
    public function getParentsChainNames() {
        if (empty($this->parentsChain)) {
            return '';
        }

        $array = [];

        foreach ($this->parentsChain as $parent) {
            $array[] = $parent->name;
        }

        return implode(' / ', $array);
    }

    /**
     * @param string $path
     * @return array
     */
    public function getBreadcrumbsChain($path) {
        $chain = $this->getParentsChain();
        $chain[] = $this;
        $breadcrumbs = [];

        foreach ($chain as $model) {
            $breadcrumbs[] = [
                'label' => $model->name,
                'url' => [
                    $path,
                    'slug' => $model->slug,
                ],
            ];
        }

        return $breadcrumbs;
    }

    /**
     * @return array
     */
    public function getParentsChainIds() {
        if (empty($this->parentsChain)) {
            return [];
        }

        $array = [];

        foreach ($this->parentsChain as $parent) {
            $array[] = $parent->id;
        }

        return $array;
    }

}
