<?php

namespace app\models;
use app\models\traits\ModelsListTrait;

use Yii;

/**
 * This is the model class for table "parameter".
 *
 * @property int $id
 * @property string $label
 * @property string $name
 *
 * @property FeatureValue[] $featureValues
 */
class Parameter extends \yii\db\ActiveRecord
{
    
    use ModelsListTrait;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parameter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['label', 'name'], 'required'],
            [['label', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatureValues()
    {
        return $this->hasMany(FeatureValue::className(), ['parameter_id' => 'id']);
    }
}
