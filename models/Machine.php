<?php

namespace app\models;

use Yii;
use app\models\Parameter;
use app\models\Feature;
use app\models\FeatureValue;
use app\components\RequestApi;
use app\helpers\FormatterHelper;
use app\helpers\Wialon;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use app\models\Statistics;

/**
 * This is the model class for table "machine".
 *
 * @property int $id
 * @property string $label
 *
 * @property Category[] $categories
 * @property FeatureValue[] $featureValues
 */
class Machine extends \yii\db\ActiveRecord {

    public $info = [];
    public $mainImage;
    public $schemaImage;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'machine';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['label', 'wialon_id', 'wialon_name'], 'required'],
                [['wialon_id', 'stats_update'], 'integer'],
                [['label', 'wialon_name'], 'string', 'max' => 255],
                [['sensors'], 'string'],
                [['mainImage', 'schemaImage'], 'file', 'extensions' => 'png'],
                [['info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'label' => 'Полное название единицы техники',
            'mainImage' => 'Основное изображение',
            'schemaImage' => 'Схема состояния',
        ];
    }

    public static function getCurrent() {
        $machineId = (Yii::$app->user->can('admin')) ?
                Yii::$app->request->cookies['machineId']->value : Yii::$app->user->identity->machine_id;
        return self::findOne($machineId);
    }

    public static function getMachineList() {
        $models = parent::find()->select(['id', 'wialon_name'])->where(['>', 'id', 1])->all();
        return ArrayHelper::map($models, 'id', 'wialon_name');
    }

    public function getFeatureByName($featureName) {
        return Feature::findOne(['machine_id' => $this->id, 'name' => $featureName]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories() {
        return $this->hasMany(Category::className(), ['machine_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatistics() {
        return $this->hasMany(Statistics::className(), ['machine_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatureValues() {
        return $this->hasMany(FeatureValue::className(), ['machine_id' => 'id']);
    }

    public static function getMachinesFromWialon() {
        $result = [];
        $items = Wialon::searchItems("avl_unit");
        foreach ($items['items'] as $key => $item) {
            $result[$item['id']]['name'] = $item['nm'];
            $result[$item['id']]['isAdd'] = (self::findOne(['wialon_id' => $item['id']]) ? true : false);
        }
        return $result;
    }

    public function getSensors() {
        $requestApi = new RequestApi();
        $from = time() - 10;
        $to = time() - 10;
        $report = Wialon::execReport($requestApi, $this->wialon_id, 1, $from, $to);
        $resultRows = Wialon::getResultRow($requestApi, $this->wialon_id, 1, $from, $to);
        if (isset($resultRows["error"]) || !$resultRows) {
            $from = $this->sensors->time;
            $report = Wialon::execReport($requestApi, $this->wialon_id, 1, $from, $to);
            $resultRows = Wialon::getResultRow($requestApi, $this->wialon_id, 1, $from, $to);
            if (isset($resultRows["error"]) || !$resultRows) {
                return false;
            }
        }
        $sensors = FormatterHelper::formatSensors($resultRows, $report);
        $this->sensors = FormatterHelper::prepareSensors($sensors);
        return $this->save();
    }
    
    public function _getRoute($from, $to) {
        $requestApi = new RequestApi();
        $loadMessages = Wialon::loadMessages($requestApi, $this->wialon_id, $from, $to);
        $result = [];
        if (isset($loadMessages['count']) && $loadMessages['count']) {
            $step = self::getStep($loadMessages['count']);
            $speed = 0;
            for ($i = 0; $i < $loadMessages['count']; $i += $step) {
                $message = Wialon::getMessages($requestApi, $i, $i + 1);
                $result['points'][$i][] = $message[0]['pos']['x'];
                $result['points'][$i][] = $message[0]['pos']['y'];
                $result['points'][$i][] = $message[0]['pos']['s'];
                $result['points'][$i][] = $message[0]['t'];
                $speed += $message[0]['pos']['s'];
            }
            $messageLayer = Wialon::createMessagesLayer($requestApi, $this->wialon_id, $from, $to);
            $result['stat']['mileage'] = isset($messageLayer['units']) ? $messageLayer['units'][0]['mileage'] : 0;
            $result['stat']['speed'] = $speed / ($loadMessages['count'] / $step);
        }
        return $result;
    }
    
    public function getRoute($from, $to) {
        $result['points'] = [];
        $result['stat']['mileage'] = 0;
        $result['stat']['speed'] = 0;
        $countSpeedMark = 0;
        $step = 100000;

        $countPoints = Statistics::find()
                ->where(['machine_id' => $this->id])
                ->andWhere(['>', 'time', $from])
                ->andWhere(['<', 'time', $to])
                ->distinct()
                ->asArray()
                ->count();

        for ($i = $step; $i < $countPoints; $i += $step) {
            $points = Statistics::find()
                ->where(['machine_id' => $this->id])
                ->andWhere(['>', 'time', $from])
                ->andWhere(['<', 'time', $to])
                ->limit($i - $step, $i)
                ->distinct()
                ->asArray()
                ->all();        
            
            if($points) {
                foreach($points as $key => $point) {
                    if ($key && $point['x'] != $points[$key - 1]['x'] && $point['y'] != $points[$key - 1]['y']) {
                        $result['points'][$key][] = (float) $point['x'];
                        $result['points'][$key][] = (float) $point['y'];
                        $result['points'][$key][] = $point['speed'];
                        $result['points'][$key][] = $point['time'];
                    }
                    if ($point['speed'] > 0) {
                        $countSpeedMark++;
                    }
                }
            }
        }
        if($result['points']) {
            $requestApi = new RequestApi();
            Wialon::loadMessages($requestApi, $this->wialon_id, $from, $to);
            $messageLayer = Wialon::createMessagesLayer($requestApi, $this->wialon_id, $from, $to);
            $result['stat']['mileage'] = isset($messageLayer['units']) ? $messageLayer['units'][0]['mileage'] : 0;
            $result['stat']['speed'] = ($countSpeedMark) ? $result['stat']['speed'] / $countSpeedMark : $result['stat']['speed'];            
        }
        
        return $result;
    }
    
    public function getRoute1($from, $to) {        
        $points = Yii::$app->db->createCommand('SELECT `x`, `y`, MAX(`time`), MAX(`speed`)
            FROM   statistics
            WHERE machine_id = :machine_id
            AND `time` > :from
            AND `time` < :to
            GROUP BY `x`')
            ->bindValue(':machine_id', $this->id)
            ->bindValue(':from', $from)
            ->bindValue(':to', $to)
            ->queryAll();
        $result['points'] = [];
        $result['stat']['mileage'] = 0;
        $result['stat']['speed'] = 0;
        $countSpeedMark = 0;
        if($points) {
            foreach($points as $key => $point) {
                $result['points'][$point['MAX(`time`)']][] = (float) $point['x'];
                $result['points'][$point['MAX(`time`)']][] = (float) $point['y'];
                $result['points'][$point['MAX(`time`)']][] = $point['MAX(`speed`)'];
                $result['points'][$point['MAX(`time`)']][] = $point['MAX(`time`)'];
                if ($point['MAX(`speed`)'] > 0) {
                    $result['stat']['speed'] += $point['MAX(`speed`)'];
                    $countSpeedMark++;
                }
            }            
            ksort($result['points']);
            //var_dump($result['points']); die;
            $requestApi = new RequestApi();
            Wialon::loadMessages($requestApi, $this->wialon_id, $from, $to);
            $messageLayer = Wialon::createMessagesLayer($requestApi, $this->wialon_id, $from, $to);
            $result['stat']['mileage'] = isset($messageLayer['units']) ? $messageLayer['units'][0]['mileage'] : 0;
            $result['stat']['speed'] = ($countSpeedMark) ? $result['stat']['speed'] / $countSpeedMark : $result['stat']['speed'];
            $result['stat']['engine_hours'] = $this->getEngineHours($from, $to);
            $result['stat']['driving_time'] = $this->getDrivingTime($from, $to);
        }
        return $result;
    }
    
    public function getEngineHours($from, $to) {
        $tact = Statistics::find()
                ->where(['machine_id' => $this->id])
                ->andWhere(['>', 'time', $from])
                ->andWhere(['<', 'time', $to])
                ->andWhere(['>=', 'engine_speed', 150])
                ->asArray()
                ->count();
        return $tact / 3600;
    }
    
    public function getDrivingTime($from, $to) {
        $tact = Statistics::find()
                ->where(['machine_id' => $this->id])
                ->andWhere(['>', 'time', $from])
                ->andWhere(['<', 'time', $to])
                ->andWhere(['>', 'speed', 0])
                ->asArray()
                ->count();
        return $tact / 3600;
    }

    public function getStats() {
        $requestApi = new RequestApi();
        while ((time() - $this->stats_update) > 60) {
            $from = $this->stats_update;            
            $to = $this->stats_update + 36000;
            $requestApi = new RequestApi();
            $loadMessages = Wialon::loadMessages($requestApi, $this->wialon_id, $from, $to, 0xffffffff);
            if (isset($loadMessages['count']) && $loadMessages['count'] > 0) {
                foreach ($loadMessages['messages'] as $message) {
                    if (Statistics::findOne(['time' => $message['t'], 'machine_id' => $this->id])) {
                        $this->stats_update = $message['t'];
                        $this->save();
                        continue;
                    }
                    $statistics = new Statistics();
                    $statistics->time = $message['t'];
                    $statistics->speed = $message['pos']['s'];
                    $statistics->x = $message['pos']['x'];
                    $statistics->y = $message['pos']['y'];
                    $statistics->engine_speed = $message['p']['adc1'] * 60000 / 2;
                    $statistics->machine_id = $this->id;
                    $statistics->save();
                    $this->stats_update = $message['t'];
                    $this->save();
                }
            }
            $this->stats_update = $to;
            $this->save();
        }        
    }

    public static function chooseDefault() {
        $machine = self::find()->where(['>', 'id', 1])->one();
        self::choose($machine->id);
    }

    public static function choose($id) {
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'machineId',
            'value' => $id,
        ]));
    }

    public function afterFind() {
        parent::afterFind();
        $this->sensors = json_decode($this->sensors);
    }

    public function beforeValidate() {
        if (!is_string($this->sensors)) {
            $this->sensors = json_encode($this->sensors);
        }
        return parent::beforeValidate();
    }

    public function uploadImages() {
        $this->mainImage = UploadedFile::getInstance($this, 'mainImage');
        $this->schemaImage = UploadedFile::getInstance($this, 'schemaImage');
        if ($this->validate()) {
            if ($this->mainImage)
                $this->mainImage->saveAs("images/{$this->id}-mainImage.{$this->mainImage->extension}");
            $this->mainImage = null;
            if ($this->schemaImage)
                $this->schemaImage->saveAs("images/{$this->id}-schemaImage.{$this->schemaImage->extension}");
            $this->schemaImage = null;
        } else {
            return false;
        }
        return true;
    }

    public static function getStep($count) {
        $result = 100;
        switch ($count) {
            case ($count < 1000):
                $result = 10;
                break;
            case ($count < 10000):
                $result = 100;
                break;
            case ($count < 50000):
                $result = 500;
                break;
            case ($count < 100000):
                $result = 1000;
                break;
            case ($count < 500000):
                $result = 5000;
                break;
            case ($count < 1000000):
                $result = 10000;
                break;
        }
        return $result;
    }

    public function saveInfo() {
        $params = Parameter::getModelsByName();
        foreach ($this->info as $category) {
            foreach ($category as $featureValue) {
                if (isset($featureValue['defaultValue'])) {
                    $featureValue['defaultValue'] = str_replace(',', '.', $featureValue['defaultValue']);
                }
                if ($featureValue['featureId']) {
                    $featureModel = Feature::findOne($featureValue['featureId']);
                    $featureModel->label = $featureValue['featureLabel'];
                    $featureModel->save();
                } else {
                    $featureModel = new Feature();
                    $featureModel->label = $featureValue['featureLabel'];
                    $featureModel->category_id = $featureValue['categoryId'];
                    $featureModel->save();
                    if (isset($featureValue['defaultValue'])) {
                        FeatureValue::create($featureModel->id, 1, $featureValue['defaultValue']);
                    } else {
                        FeatureValue::create($featureModel->id, 2, $featureValue['modelValue']);
                        FeatureValue::create($featureModel->id, 3, $featureValue['modelNumberValue']);
                    }
                }
                if (isset($featureValue['defaultId']) && $featureValue['defaultId']) {
                    $featureValueModel = FeatureValue::findOne($featureValue['defaultId']);
                    if ($featureModel->name == 'engine_hours') {
                        if (round($featureValueModel->value / (60 * 60), 1) != $featureValue['defaultValue']) {
                            $featureValueModel->value = (string) ($featureValue['defaultValue'] * 60 * 60);
                        }
                    } else {
                        $featureValueModel->value = $featureValue['defaultValue'];
                    }
                    $featureValueModel->save();
                }
                if (isset($featureValue['modelId']) && $featureValue['modelId']) {
                    $featureValueModel = FeatureValue::findOne($featureValue['modelId']);
                    $featureValueModel->value = $featureValue['modelValue'];
                    $featureValueModel->save();
                }
                if (isset($featureValue['modelNumberId']) && $featureValue['modelNumberId']) {
                    $featureValueModel = FeatureValue::findOne($featureValue['modelNumberId']);
                    $featureValueModel->value = $featureValue['modelNumberValue'];
                    $featureValueModel->save();
                }
            }
        }
    }

}

