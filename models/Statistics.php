<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "statistics".
 *
 * @property int $id
 * @property int $machine_id
 * @property int $time
 * @property double $x
 * @property double $y
 * @property int $speed
 * @property int $engine_speed
 *
 * @property Machine $machine
 */
class Statistics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'statistics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['machine_id'], 'required'],
            [['machine_id', 'time', 'speed', 'engine_speed'], 'integer'],
            [['x', 'y'], 'number'],
            [['machine_id'], 'exist', 'skipOnError' => true, 'targetClass' => Machine::className(), 'targetAttribute' => ['machine_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'machine_id' => 'Machine ID',
            'time' => 'Time',
            'x' => 'X',
            'y' => 'Y',
            'speed' => 'Speed',
            'engine_speed' => 'Engine Speed',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMachine()
    {
        return $this->hasOne(Machine::className(), ['id' => 'machine_id']);
    }
}
