<table class="table table-sm table-bordered">
    <thead>
        <tr>
            <td  colspan="2">
                <b><?= $category->label ?></b>
            </td> 
        </tr>
        <?php foreach ($category->features as $feature) : ?>
            <?php if (!$feature->is_calc) : ?>
                <tr>
                    <td>
                        <?= $feature->label ?>
                    </td>
                    <td clas="text-right">
                        <?= $feature->featureValues[0]->value; ?>
                    </td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </thead>
    <tbody>
        <?php foreach ($category->categories as $subcategory) : ?>        
            <tr>
                <td  colspan="2">
                    <b><?= $subcategory->label ?></b>
                </td>
            </tr>
            <?php foreach ($subcategory->features as $feature) : ?>
                <?php if (!$feature->is_calc) : ?>
                    <tr>
                        <td class="text-right">
                            <?= $feature->label ?>
                        </td>
                        <td class="text-right">
                            <?= $feature->featureValues[0]->value; ?>
                        </td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?> 
        </tbody>        
    <?php endforeach; ?>
</table>