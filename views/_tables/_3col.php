<table class="table table-sm table-bordered">
    <thead>
        <tr>
            <td  colspan="3">
                <b><?= $category->label ?></b>
            </td> 
        </tr>
        <?php foreach ($category->features as $feature) : ?>
            <?php if (!$feature->is_calc) : ?>
                <tr>
                    <td>
                        <?= $feature->label ?>
                    </td>
                    <td  colspan="2" class="text-right">
                        <?= $feature->featureValues[0]->value; ?>
                    </td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </thead>
    <tbody>
        <?php foreach ($category->categories as $subcategory) : ?>    
            <tr>
                <td>
                    <b><?= $subcategory->label ?></b>
                </td>
                <td class="text-right">
                    <b>Марка / Модель</b>
                </td>
                <td class="text-right">
                    <b>Номер</b>
                </td>
            </tr>
            <?php foreach ($subcategory->features as $feature) : ?>
                <?php if (!$feature->is_calc) : ?>
                    <tr>
                        <td>
                            <?= $feature->label ?>
                        </td>
                        <td class="text-right">
                            <?= $feature->featureValues[0]->value; ?>
                        </td>
                        <td class="text-right">
                            <?= $feature->featureValues[1]->value; ?>
                        </td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?> 
        </tbody>
    <?php endforeach; ?>
</table>