<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use app\components\widgets\ChooseMachineWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap navbar">
    <header>
        <h1><?= Html::encode($this->title) ?></h1>        
        <?= Html::a(
            Html::tag(
                'div', 
                Html::tag('span', 'Акционерное Общество', ['class' => 'org-form']) .
                Html::tag('span', 'КурганАвтоРемонт', ['class' => 'org-name']),
                ['class' => 'title']
            ).
            Html::img(['images/logo.png']),
            ['/site/index'],
            ['class' => 'logo']
        );?>
        <?= ChooseMachineWidget::widget() ?>
    </header>
    <?php
    NavBar::begin([
        'options' => [
            'class' => 'navbar-custom',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
            ['label' => 'Главная', 'url' => ['/monitoring/index']],
            ['label' => 'Информация', 'url' => ['/monitoring/info']],
            ['label' => 'Состояние', 'url' => ['/monitoring/condition']],
            ['label' => 'Маршрут', 'url' => ['/monitoring/route']],
            ['label' => 'Бурение', 'url' => ['#']],
            ['label' => 'Статистика', 'url' => ['#']],
            ['label' => 'Ресурс', 'url' => ['#']],
            ['label' => 'Обслуживание', 'url' => ['#']],
            ['label' => 'Корректировка', 'url' => [(Yii::$app->user->can('admin')) ? '/admin' : '#' ]],
            Yii::$app->user->isGuest ? (
                ['label' => 'Авторизация', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Выход (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
