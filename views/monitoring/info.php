<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Информация';
?>
<div class="monitoring-info">
    <div class="machine-label">
        <?= $model->label ?>
    </div>
    <div class="container">
        <div class="col-md-4">
            <?=
            $this->render('@app/views/_tables/_3col', [
                'category' => $categories[0],
            ])
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $this->render('@app/views/_tables/_3col', [
                'category' => $categories[1],
            ])
            ?>
            <?=
            $this->render('@app/views/_tables/_3col', [
                'category' => $categories[2],
            ])
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $this->render('@app/views/_tables/_2col', [
                'category' => $categories[3],
            ])
            ?>
        </div>            
    </div>
</div>
