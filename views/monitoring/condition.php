<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\Pjax;
$this->title = 'Состояние';
?>
<div class="monitoring-condition">
    <div class="machine-label">
        <?= $model->label ?><br>        
    </div>
    <?php Pjax::begin(); ?>
    <?= Html::a("Обновить", ['monitoring/condition'], ['class' => 'hidden', 'id' => 'refreshButton']) ?>
    <p>Последнее обновление данных: <?= Yii::$app->formatter->asRelativeTime($model->sensors->time) ?></p>
    <div class="col-md-3">
        <h2>Текущее состояние систем</h2>
        <div class="section">
            <h3>Снегоболотоход</h3>
            <div class="row">
                <div class="col-md-5">
                    Координаты: 
                </div>
                <div class="col-md-7">
                    <span><?= $model->sensors->lon ?><br><?= $model->sensors->lat ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    Moточасы: 
                </div>
                <div class="col-md-7">
                    <span><?= round($model->getFeatureByName('engine_hours')->featureValues[0]->value / (60*60), 1) ?></span>
                </div>
            </div>
            <mark class="lightgreen-color">Двигатель: <?= $model->getFeatureByName('engine')->featureValues[0]->value ?></mark>
            <div class="row">
                <div class="col-md-8">
                    Обороты, об.мин
                </div>
                <div class="col-md-4">
                    <span><?= $model->getFeatureByName('c_engine_speed')->featureValues[0]->value * $model->sensors->sens5002 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    Система смазки
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Давление, кгс/см<sup>2</sup>
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5022 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Температура, °С
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5019 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    Система охлаждения
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Температура, °С
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5018 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    Система забор воздуха
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Фильтр воздушный
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5023 ?></span>
                </div>
            </div>
            <mark class="lightgreen-color">Транcмиссия:</mark>
            <div class="row">
                <div class="col-md-12">
                    Система смазки
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Давление, кгс/см<sup>2</sup>
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5021 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Температура, °С
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5020 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    Редуктор отбора мощности
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Температура, °С
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5013 ?></span>
                </div>
            </div>
        </div>
        <table>
            <tr>
                <td><mark class="yellow-color">Д1</mark></td>
                <td>Положение вращателя</td>
            </tr>
            <tr>
                <td><mark class="yellow-color">Д2</mark></td>
                <td>Обороты вращателя</td>
            </tr>
            <tr>
                <td><mark class="yellow-color">Д3</mark></td>
                <td>Обороты двигателя</td>
            </tr>
            <tr>
                <td><mark class="green-color">ДД12</mark></td>
                <td>Давление системы смазки ГП</td>
            </tr>
            <tr>
                <td><mark class="green-color">ДД13</mark></td>
                <td>Давление системы смазки ДВС</td>
            </tr>
            <tr>
                <td><mark class="cyan-color">ДТ3</mark></td>
                <td>Температура РОМ</td>
            </tr>
            <tr>
                <td><mark class="orange-color">ДТ5</mark></td>
                <td>Температура ОЖ двигателя</td>
            </tr>
            <tr>
                <td><mark class="orange-color">ДТ6</mark></td>
                <td>Температура масла двигателя</td>
            </tr>
            <tr>
                <td><mark class="orange-color">ДТ7</mark></td>
                <td>Температура масла ГП</td>
            </tr>
            <tr>
                <td><mark class="fuchsia-color"> ДФБ</mark></td>
                <td>Загрязнение воздушн. фильтра</td>
            </tr>
        </table>
    </div>
    <div class="col-md-6">
        <?= Html::img(['images/' . $model->id . '-schemaImage.png']); ?>
    </div>
    <div class="col-md-3">
        <div class="section">
            <h3>Буровая установка</h3>
            <mark class="lightgreen-color">Вращатель:</mark>
            <div class="row">
                <div class="col-md-8">
                    Обороты, об/мин
                </div>
                <div class="col-md-4">
                    <span><?= $model->getFeatureByName('c_rotator_speed')->featureValues[0]->value * $model->sensors->sens5001 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Крутящий момент, кг х м
                </div>
                <div class="col-md-4">
                    <span><?= $model->getFeatureByName('с_torque')->featureValues[0]->value * ($model->sensors->sens5007 - $model->sensors->sens5008) ?></span>
                </div>
            </div>
            <mark class="lightgreen-color">Подача вращателя:</mark>
            <div class="row">
                <div class="col-md-8">
                    Положение, мм
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5000 - $model->getFeatureByName('zero_rotator_position')->featureValues[0]->value ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Усилие подачи, кгс
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5009 ?></span>
                </div>
            </div>
            <mark class="lightgreen-color">Гидросистема:</mark>  
            <div class="row">
                <div class="col-md-12">
                    Давление в контурах, кгс/см<sup>2</sup>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Контур ГСТ(прям.)
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5007 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Контур ГСТ(обр.)
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5008 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Контур подачи
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5009 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Контур управления
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5010 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    Температура в контурах, °С
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Контур ГСТ
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5011 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Контур сливной
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5012 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    Загрязнение фильтров
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Напорный фильтр 1
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5014 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Напорный фильтр 2
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5015 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Сливной фильтр
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5016 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    Сигналы
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Уровень масла г/бака
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5017 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Мотор-насос ВПУ
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5004 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    М/охладитель г/системы
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5005 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Ограждение раб. зоны
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5003 ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Отсек ВМ
                </div>
                <div class="col-md-4">
                    <span><?= $model->sensors->sens5006 ?></span>
                </div>
            </div>
        </div>
        <table>
            <tr>
                <td><mark class="royalblue-color">ДД1</mark></td>
                <td>Давление контура ГСТ (прям.)</td>
            </tr>
            <tr>
                <td><mark class="royalblue-color">ДД2</mark></td>
                <td>Давление контура ГСТ (обр.)</td>
            </tr>
            <tr>
                <td><mark class="royalblue-color">ДД3</mark></td>
                <td>Давление контура подачи</td>
            </tr>
            <tr>
                <td><mark class="royalblue-color">ДД4</mark></td>
                <td>Давление контура управления</td>
            </tr>
            <tr>
                <td><mark class="cyan-color">ДТ1</mark></td>
                <td>Температура г/системы ГСТ</td>
            </tr>
            <tr>
                <td><mark class="cyan-color">ДТ2</mark></td>
                <td>Температура г/системы слив</td>
            </tr>
        </table>
    </div>
</div>
<?php Pjax::end(); ?>

<?php
$script = <<< JS
$(document).ready(function() {
    setInterval(function(){ $("#refreshButton").click(); }, 30000);
});
JS;
$this->registerJs($script);
?>