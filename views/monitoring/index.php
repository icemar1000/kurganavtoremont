<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
$this->title = 'Система мониторинга';
?>
<div class="monitoring-index">
    <div class="row">
        <div class="col-md-6">
            <?= Html::img(['images/' . $model->id . '-mainImage.png']); ?>
        </div>
        <div class="col-md-6 info">
            <div class="row title">
                <?= $model->label ?>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-10">
                <div class="row">
                    <p><?= $model->getFeatureByName('chassis_name')->featureValues[0]->value ?></p>
                    <p><span><?= $model->getFeatureByName('chassis_number')->featureValues[0]->value ?></span></p>
                    <p><?= $model->getFeatureByName('rig_name')->featureValues[0]->value ?></p>
                    <p><span><?= $model->getFeatureByName('rig_number')->featureValues[0]->value ?></span></p>
                </div>
                <div class="row">
                    Координаты:
                    <p><span><?= $model->sensors->lon ?></span></p>
                    <p><span><?= $model->sensors->lat ?></span></p>
                </div>
                <div class="row">
                    <p>
                    <?php foreach($model->sensors->address as $address): ?>
                        <?= $address ?><br>
                    <?php endforeach; ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
