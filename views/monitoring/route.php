<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\datetime\DateTimePicker;
$this->registerJsFile('https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js');
$this->title = 'Система мониторинга';

$points = 0;
$stat = [];
$now = 0;
if ($interval) {
    $_route = $model->getRoute($interval['from'], $interval['to']);
    $route = ($_route['points']) ? $_route : $model->_getRoute($interval['from'], $interval['to']);
    $points = (isset($route['points'])) ? json_encode($route['points']) : '[]';
    $stat = (isset($route['stat'])) ? $route['stat'] : [];
    $now = ((time() - $interval['to']) < 60 * 60 * 24) ? 1 : 0;
}
?>
<div class="monitoring-route">
    <div class="row">        
        <div class="col-md-8">
            <div id="map" class="map">
                <div id="info"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="title">
                <?= $model->label ?>
            </div>
            <div class="section">
                <p>Период</p>
                <div class="row table-preset-interval">
                    <div class="col-md-3">
                        <a class="preset-interval" id="today">Сегодня</a>
                    </div>
                    <div class="col-md-3">
                        <a class="preset-interval" id="yesterday">Вчера</a>
                    </div>
                    <div class="col-md-3">
                        <a class="preset-interval" id="week">Неделя</a>
                    </div>
                    <div class="col-md-3">
                        <a class="preset-interval" id="month">Месяц</a>
                    </div>
                </div>
                <?= Html::beginForm(['route'], 'post', ['id' => 'choose-interval']) ?>
                <div class="row">                    
                    <div class="col-md-3">
                        Интервал:
                    </div>
                    <div class="col-md-9">
                        <?= Html::dropDownList('typeInterval', ($interval) ? $interval['typeInterval'] : '', [
                                'interval' => 'Указанный интервал',
                                'fromToday' => 'Начиная "От" до сегодня',
                                'timeAgo' => 'За предыдущие'
                            ], 
                            [
                                'class' => 'form-control',
                                'id' => 'choose-type-interval'
                        ]); ?>
                    </div>
                </div>
                <div id="date-pick" class="<?= ($interval && $interval['typeInterval'] == 'timeAgo') ? 'hidden' : '' ?>">
                    <div class="row">                    
                        <div class="col-md-3">
                            От:
                        </div>
                        <div class="col-md-9">
                            <?php
                            echo DateTimePicker::widget([
                                'name' => 'choose-interval-from',
                                'language' => 'ru',
                                'type' => DateTimePicker::TYPE_INPUT,
                                'value' => date('d-m-Y H:i', ($interval) ? $interval['from'] : mktime(0, 0, 0)),
                                'options' => [
                                    'id' => 'choose-interval-from',
                                    'placeholder' => 'Выберите дату...'
                                ],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd-mm-yyyy hh:ii',
                                    'todayHighlight' => true,
                                    'minuteStep' => 10,
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-md-3">
                            До:
                        </div>
                        <div class="col-md-9">
                            <?php
                            echo DateTimePicker::widget([
                                'name' => 'choose-interval-to',
                                'language' => 'ru',
                                'type' => DateTimePicker::TYPE_INPUT,
                                'disabled' => ($interval && $interval['typeInterval'] == 'fromToday') ? true : false,
                                'value' => date('d-m-Y H:i', ($interval) ? $interval['to'] : time()),
                                'options' => [
                                    'id' => 'choose-interval-to',
                                    'placeholder' => 'Выберите дату...'
                                ],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd-mm-yyyy hh:ii',
                                    'todayHighlight' => true,
                                    'minuteStep' => 10,
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="<?= (!$interval || ($interval && $interval['typeInterval'] != 'timeAgo')) ? 'hidden' : '' ?>" id="timeAgo-pick">
                    <div class="row">                    
                        <div class="col-md-3 col-md-offset-6">
                            <?=
                            Html::textInput('periodLength', ($interval) ? $interval['periodLength'] : 1, [
                                'id' => 'choose-period-length',
                                'type' => 'number',
                                'class' => 'form-control',
                                'min' => 1
                            ])
                            ?>

                        </div>
                        <div class="col-md-3">
                            <?=
                            Html::dropDownList('periodType', ($interval) ? $interval['periodType'] : '', [
                                60 => 'минут',
                                60 * 60 => 'часов',
                                60 * 60 * 24 => 'дней',
                                60 * 60 * 24 * 7 => 'недель',
                                60 * 60 * 24 * 30 => 'месяцев',
                                60 * 60 * 24 * 365 => 'лет',
                                    ], [
                                'class' => 'form-control',
                                'style' => 'margin-left: -25px; width: 87px;',
                                'id' => 'choose-period-type'
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1 col-md-offset-6">
                            <?= Html::checkbox('currentInterval'); ?>
                        </div>
                        <div class="col-md-5">
                            <p style="padding-top: 5px; margin-left: -12px;">От текущего интервала</p>
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 right-align">
                        <?= Html::submitButton('Построить трек', ['class' => '', 'id' => 'choose-interval-button']); ?>
                    </div>
                </div>
                <?= Html::endForm() ?>
                <div class="row">
                    <div class="col-md-6">
                        <?= $model->getFeatureByName('chassis_name')->featureValues[0]->value ?>
                    </div>
                    <div class="col-md-6 underline right-align">
                        <?= $model->getFeatureByName('chassis_number')->featureValues[0]->value ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $model->getFeatureByName('rig_name')->featureValues[0]->value ?>
                    </div>
                    <div class="col-md-6 underline right-align">
                        <?= $model->getFeatureByName('rig_number')->featureValues[0]->value ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        Текущие координаты:
                    </div>
                    <div class="col-md-6 underline">
                        <?= $model->sensors->lon ?> : <?= $model->sensors->lat ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        Пройдено расстояние, км
                    </div>
                    <div class="col-md-6 underline">
                        <?= (isset($stat['mileage'])) ? round($stat['mileage']/1000) : 0 ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        Средняя скорость, км/ч
                    </div>
                    <div class="col-md-6 underline">
                        <?= (isset($stat['speed'])) ? round($stat['speed']) : 0 ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        Время работы двигателя, часов
                    </div>
                    <div class="col-md-6 underline">
                        <?= (isset($stat['engine_hours'])) ? round($stat['engine_hours'], 1) : 0 ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        Время движения, часов
                    </div>
                    <div class="col-md-6 underline">
                        <?= (isset($stat['driving_time'])) ? round($stat['driving_time'], 1) : 0 ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php
    $x = $model->sensors->x;
    $y = $model->sensors->y;
    $iconUrl = Url::to(['images/marker.png']);    
    $script = <<< JS
//выбор предустановленного временного интервала
$(".preset-interval").on("click", function() {
    let date = new Date();
    switch(this.id) {
        case 'today':
            $("#choose-type-interval").val('interval');
            $("#choose-interval-to").val(date.toLocaleString("ru"));
            date.setHours(0, 0, 0, 0);
            $("#choose-interval-from").val(date.toLocaleString("ru"));
            break;
        case 'yesterday':
            $("#choose-type-interval").val('interval');
            date.setHours(0, 0, 0, 0);
            $("#choose-interval-to").val(date.toLocaleString("ru"));
            date.setDate(date.getDate() - 1);
            $("#choose-interval-from").val(date.toLocaleString("ru"));
            break;
        case 'week':
            $("#choose-type-interval").val('timeAgo');
            $("#choose-period-length").val(1);
            $("#choose-period-type").val(60 * 60 * 24 * 7);
            break;
        case 'month':
            $("#choose-type-interval").val('timeAgo');
            $("#choose-period-length").val(1);
            $("#choose-period-type").val(60 * 60 * 24 * 30);
            break;
   };
   $('#choose-interval-button').click();
});
            
//изменение вида формы
$("#choose-type-interval").change(function(){
    switch ($(this).val()) {
        case 'interval':
            console.log($(this).val());
            $('#date-pick').removeClass('hidden');
            $('#timeAgo-pick').addClass('hidden');
            $("#choose-interval-to").prop("disabled", false);
            break;
        case 'fromToday':
            console.log($(this).val());
            $('#date-pick').removeClass('hidden');
            $('#timeAgo-pick').addClass('hidden');
            $("#choose-interval-to").prop("disabled", true);
            break;
        case 'timeAgo':
            console.log($(this).val());
            $('#date-pick').addClass('hidden');
            $('#timeAgo-pick').removeClass('hidden');
            break;
    }        
});
            
//создание карты            
var iconFeature = new ol.Feature({
  geometry: new ol.geom.Point(ol.proj.transform([{$x}, {$y}], 'EPSG:4326', 'EPSG:3857')),
  coords: 'Island',
  date: 4000,
  speed: 500
});

var iconStyle = new ol.style.Style({
  image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    anchor: [0.5, 46],
    anchorXUnits: 'fraction',
    anchorYUnits: 'pixels',
    opacity: 1,
    src: '{$iconUrl}'
  }))
});

iconFeature.setStyle(iconStyle);

var vectorSource = new ol.source.Vector({
  features: [iconFeature]
});

var vectorLayer = new ol.layer.Vector({
  source: vectorSource
});
    
        var map = new ol.Map({
        controls: ol.control.defaults({
            zoom: true,
            attribution: false,
            rotate: false
        }).extend([
            new ol.control.ScaleLine()
        ]),
        layers: [new ol.layer.Tile({ source: new ol.source.OSM() }), vectorLayer],
        target: document.getElementById('map'),
        view: new ol.View({
            //center: ({$points} && points.length > 0) ? ol.proj.fromLonLat([points[points.length - 1][0], points[points.length - 1][1]]) : ol.proj.fromLonLat([{$x}, {$y}]),
            center: ol.proj.fromLonLat([{$x}, {$y}]),
            zoom: 3
        })
    }); 
    
//добавление трека
if({$points}) {
    var points = []; 
    var _points = {$points};
    if(_points.length == 0) {
        setTimeout(function() {alert("Нет данных за выбранный период")}, 1000);
    }
    console.log(_points);
    
    var vectorLine = new ol.source.Vector({});
    for (let key in _points) {
        points[points.length] = ol.proj.transform(_points[key], 'EPSG:4326', 'EPSG:3857');
        
        var featureCircle = new ol.Feature({
            geometry: new ol.geom.Point(points[points.length - 1]),
            x: _points[key][0],
            y: _points[key][1],
            date: new Date(_points[key][3] * 1000),
            speed: Math.round(_points[key][2])
        });
    
        vectorLine.addFeature(featureCircle);
    }
    console.log(points);
    
    /*if($now) {
        points[points.length] = ol.proj.transform([{$x}, {$y}], 'EPSG:4326', 'EPSG:3857');
    } */    

        
    var featureLine = new ol.Feature({
        geometry: new ol.geom.LineString(points)
    });

    
    vectorLine.addFeature(featureLine);
        
    var vectorLineLayer = new ol.layer.Vector({
        source: vectorLine,
        style: new ol.style.Style({
            image: new ol.style.Circle({
                radius: 4,
                fill: new ol.style.Fill({color: '#00E2E2'})
            }),
            fill: new ol.style.Fill({ color: '#00ffff', weight: 4 }),
            stroke: new ol.style.Stroke({ color: '#00ffff', width: 2 })
        })
    });
    
    map.addLayer(vectorLineLayer);
    
    //всплывающее окно
          map.on('pointermove', showInfo);

      var info = document.getElementById('info');
      function showInfo(event) {        
        var features = map.getFeaturesAtPixel(event.pixel);
        if (!features) {
          info.innerHTML = '';
          info.style.opacity = 0;
          return;
        }
        var properties = features[0].getProperties();
        info.innerHTML = '<p>' + properties.date.toLocaleString("ru") + '</p>' + 
            '<p>' + properties.speed + ' км/ч</p>' 
            + '<p>В ' + properties.y + ' : C ' + properties.x + '</p>';
        info.style.opacity = 1;
      }
}
JS;
    $this->registerJs($script, yii\web\View::POS_READY);
    