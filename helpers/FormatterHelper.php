<?php

namespace app\helpers;

class FormatterHelper
{
    /**
     * @param string $address
     * @return array
     */
    public static function formatAddress($address)
    {
        $result = [];
        foreach(explode(', ', $address) as $key => $value) {
            $result[] = $value;
        }
        return $result;
    }
    
    public static function formatCoords($position) 
    {
        $lat = (string) $position['y'];
        $lon = (string) $position['x'];
        return [
            'lon' => "C " . $lat[0] . $lat[1] . '°' . $lat[3] . $lat[4] . "'" . $lat[5] . $lat[6] . '"',
            'lat' => "В " . $lon[0] . $lon[1] . '°' . $lon[3] . $lon[4] . "'" . $lon[5] . $lon[6] . '"',
        ];
    }

    /**
     * @param array $resultRows
     * @param array $report
     * @return array
     */
    public static function formatSensors($resultRows, $report) 
    {
        $header = $report['reportResult']['tables'][0]['header'];
        $data = $resultRows[0]['c'];        
        $result = [
            "time" => $data[2]['v'],
            'x' => $data[2]['x'],
            'y' => $data[2]['y']           
        ];
        for($i = 3; $i <= 29; $i++) {
            $result[$header[$i]] = $data[$i];
        }
        return $result;
    }

    /**
     * @param array $resultRows
     * @return int
     */
    public static function calcEngineHours($resultRows) {
        $tact = 0;
        foreach($resultRows as $row) {
            if(isset($row['c']) && isset($row['c'][5]) && $row['c'][5] > 150) {
                $tact += 1;
            }
        }
        return $tact;
    }

    /**
     * @param array $sensors
     * @return array
     */
    public static function prepareSensors($sensors) 
    {
        $coords = self::formatCoords(['x' => $sensors['x'], 'y' => $sensors['y']]);
        $result = [];
        $result['time'] = $sensors['time'];
        $result['lon'] = $coords['lon'];
        $result['lat'] = $coords['lat'];
        $result['x'] = $sensors['x'];
        $result['y'] = $sensors['y'];
        $result['address'] = Wialon::getAddress($sensors['x'], $sensors['y']);
        $result['sens5000'] = round($sensors['sens5000 (Position)']);
        $result['sens5001'] = round($sensors['sens5001 (VRZ_ob)']);
        $result['sens5002'] = round($sensors['sens5002 (DVG_ob)']);
        $result['sens5003'] = self::openOrClose($sensors['sens5003 (OGRAZ)']);
        $result['sens5004'] = self::onOrOff($sensors['sens5004 (VPU)']);
        $result['sens5005'] = self::onOrOff($sensors['sens5005 (MO)']);
        $result['sens5006'] = self::openOrClose($sensors['sens5006 (VV)']);
        $result['sens5007'] = round($sensors['sens5007 (Pprm)']);
        $result['sens5008'] = round($sensors['sens5008 (Pobr)']);
        $result['sens5009'] = round($sensors['sens5009 (Ppod)']);
        $result['sens5010'] = round($sensors['sens5010 (Puprv)']);
        $result['sens5011'] = round($sensors['sens5011 (Tm_gst)']);
        $result['sens5012'] = round($sensors['sens5012 (Tgidr)']);
        $result['sens5013'] = round($sensors['sens5013 (TROM)']);
        $result['sens5014'] = self::cleanOrDirty($sensors['sens5014 (Filtr1)']);
        $result['sens5015'] = self::cleanOrDirty($sensors['sens5015 (Filtr2)']);
        $result['sens5016'] = self::cleanOrDirty($sensors['sens5016 (Filtr_sliv)']);
        $result['sens5017'] = self::getLvl($sensors['sens5017 (LVL)']);
        $result['sens5018'] = round($sensors['sens5018 (Toz_dvg)']);
        $result['sens5019'] = round($sensors['sens5019 (Tm_dvg)']);
        $result['sens5020'] = round($sensors['sens5020 (Tm_GP)']);
        $result['sens5021'] = round($sensors['sens5021 (Pm_gp)'], 1);
        $result['sens5022'] = round($sensors['sens5022 (Pm_dvg)'], 1);
        $result['sens5023'] = self::cleanOrDirty($sensors['sens5023 (Filtr_vozd)']);
        $result['sens5002_d1'] = round($sensors['sens5002_d1 (Obor_dvg)']);       
        
        return $result;
    }

    /**
     * @param int $value
     * @return string
     */
    public static function cleanOrDirty($value) 
    {
        return (round($value)) ? 'загрязн' : 'чист';
    }

    /**
     * @param int $value
     * @return string
     */
    public static function getLvl($value) 
    {
        return (round($value)) ? 'низк' : 'норм';
    }

    /**
     * @param int $value
     * @return string
     */
    public static function onOrOff($value) 
    {
        return (round($value)) ? 'вкл' : 'выкл';
    }

    /**
     * @param int $value
     * @return string
     */
    public static function openOrClose($value) 
    {
        return (round($value)) ? 'откр' : 'закр';
    }
}