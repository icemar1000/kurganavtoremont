<?php

namespace app\helpers;

use app\components\RequestApi;

class Wialon {

    /**
     * @return mixed
     */
    public static function getPosition()
    {
        $unit = self::searchItems("avl_unit");
        return $unit['items'][0]['pos'];
    }

    /**
     * @return bool
     */
    public static function searchSensors()
    {
        $svc = "core/search_items";
        $params = '{
            "spec":{
                "itemsType":"avl_unit",
                "propType":"propitemname",
                "propName":"unit_sensors",
                "propValueMask":"*",
                "sortType":"unit_sensors"
            },
            "flags":"0x00001001",
            "force":1,
            "from":0,
            "to":0
        }';
        $requestApi = new RequestApi();
        return $requestApi->request($svc, $params);
    }

    /**
     * @return bool
     */
    public static function calcSensors()
    {
        $requestApi = new RequestApi();
        self::loadMessages($requestApi);
        $svc = "unit/calc_sensors";
        $params = '{
            "source":"",
            "indexFrom":1,
            "indexTo":1,
            "unitId":18418458,
            "sensorId":19
        }';
        return $requestApi->request($svc, $params);
    }

    /**
     * @return bool
     */
    public static function calcLastMessage()
    {
        $svc = "unit/calc_last_message";
        $params = '{
            "unitId":18418458,
            "sensors":[]
        }';
        $requestApi = new RequestApi();
        return $requestApi->request($svc, $params);
    }

    //--------использумые методы--------
    
    //можно посмотреть id шаблона, ресурса, датчика или техники
    /**
     * @param string $itemsType
     * @return bool
     */
    public static function searchItems($itemsType = "avl_unit")
    {
        //$itemsType = "avl_unit";
        //$itemsType = "avl_resource";
        $svc = "core/search_items";
        $params = '{
            "spec":{
                "itemsType":"' . $itemsType . '",
                "propName":"reporttemplates",
                "propValueMask":"*",
                "sortType":"reporttemplates"
            },
            "flags":"0x00001401",
            "force":1,
            "from":0,
            "to":0
        }';
        $requestApi = new RequestApi();
        return $requestApi->request($svc, $params);
    }

    /**
     * @param null|RequestApi $requestApi
     * @param int $reportObjectId
     * @param int $reportTemplateId
     * @param int $from
     * @param int $to
     * @return bool
     */
    public static function execReport($requestApi = null, $reportObjectId, $reportTemplateId, $from, $to)
    {
        $requestApi = ($requestApi) ?: new RequestApi();
        $svc = "report/exec_report";
        $params = '{
            "reportResourceId":18406084,
            "reportTemplateId":' . $reportTemplateId . ',
            "reportObjectId":' . $reportObjectId . ',
            "reportObjectSecId":0,
            "interval":{
                "from":' . $from . ',
                  "to":' . $to . ',
                "flags":0
            }
        }';
        return $requestApi->request($svc, $params);
    }

    /**
     * @param null|RequestApi $requestApi
     * @param int $reportObjectId
     * @param int $reportTemplateId
     * @param int $from
     * @param int $to
     * @return bool
     */
    public static function getResultRow($requestApi = null, $reportObjectId, $reportTemplateId, $from, $to)
    {
        $requestApi = ($requestApi) ?: new RequestApi();
        $report = self::execReport($requestApi, $reportObjectId, $reportTemplateId, $from, $to);
        if (isset($report['reportResult']['tables'][0])) {
            $row = $report['reportResult']['tables'][0]['rows'] - 1; 
            $svc = "report/get_result_rows";
            $params = '{
                "tableIndex":0,
                "indexFrom":' . $row . ',
                "indexTo":' . $row . '
            }';
            $resultRows = $requestApi->request($svc, $params);
            return $resultRows;
        }
        return false;
    }

    /**
     * @param null|RequestApi $requestApi
     * @param int $reportObjectId
     * @param int $reportTemplateId
     * @param int $from
     * @param int $to
     * @return bool
     */
    public static function getResultRows($requestApi = null, $reportObjectId, $reportTemplateId, $from, $to) {
        $requestApi = ($requestApi) ?: new RequestApi();
        $report = self::execReport($requestApi, $reportObjectId, $reportTemplateId, $from, $to);
        if (isset($report['reportResult']['tables'][0])) {
            $row = $report['reportResult']['tables'][0]['rows'] - 1;
            $svc = "report/get_result_rows";
            $params = '{
                "tableIndex":0,
                "indexFrom":' . 0 . ',
                "indexTo":' . $row . '
            }';
            $resultRows = $requestApi->request($svc, $params);
            return $resultRows;
        }
        return false;
    }

    /**
     * @param string $lon
     * @param string $lat
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function getAddress($lon, $lat)
    {
            $coords = '[{
                "lon":' . $lon . ',
                "lat":' . $lat . '
            }]';
            $flags = '1255211008';
            $requestApi = new RequestApi();
            return FormatterHelper::formatAddress($requestApi->requestGeo($coords, $flags)[0]);
    }

    /**
     * @param null|RequestApi $requestApi
     * @param int $machineId
     * @param int $from
     * @param int $to
     * @param int $loadCount
     * @return bool
     */
    public static function loadMessages($requestApi = null, $machineId, $from, $to, $loadCount = 0)
    {
        $svc = "messages/load_interval";
        $params = '{
            "itemId":' . $machineId . ',
            "timeFrom":' . $from . ',
            "timeTo":' . $to . ',
            "flags":1,
            "flagsMask":65281,
            "loadCount":' . $loadCount . '
        }';
        $requestApi = ($requestApi) ?: new RequestApi();
        return $requestApi->request($svc, $params);
    }

    /**
     * @param null|RequestApi $requestApi
     * @param int $indexFrom
     * @param int $indexTo
     * @return bool
     */
    public static function getMessages($requestApi = null, $indexFrom, $indexTo)
    {
        $svc = "messages/get_messages";
        $params = '{
            "indexFrom":' . $indexFrom . ',
            "indexTo":' . $indexTo . ',

                "loadCount":"0xffffffff"
	}';
        $requestApi = ($requestApi) ?: new RequestApi();
        return $requestApi->request($svc, $params);
    }

    /**
     * @param null|RequestApi $requestApi
     * @param int $machineId
     * @param int $from
     * @param int $to
     * @return bool
     */
    public static function createMessagesLayer($requestApi = null, $machineId, $from, $to)
    {
        $svc = "render/create_messages_layer";
        $params = '{
		"layerName":"messages",
		"itemId":' . $machineId . ',
		"timeFrom":' . $from . ',
		"timeTo":' . $to . ',
		"tripDetector":1,
		"trackColor":"cc713cff",
		"trackWidth":5,
		"arrows":1,
		"points":0,
		"pointColor":0,
		"annotations":0
	}';
        $requestApi = ($requestApi) ?: new RequestApi();
        return $requestApi->request($svc, $params);
    }
}
