<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
?>
<?php if(Yii::$app->user->can('admin')) : ?>
<?= Html::beginForm(['/admin/machine/choose'], 'post', ['id' => 'choose-machine-form']) ?>
<?= Html::dropDownList('machineId', $selection, $machines, ['id' => 'choose-machine-input']); ?>
<?= Html::submitButton('', ['class' => 'hidden', 'id' => 'choose-machine-button']); ?>
<?= Html::endForm() ?>
<?php

$script = <<< JS
$("#choose-machine-input").change(function(){
    $("#choose-machine-button").click();
});
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
<?php endif; ?>

