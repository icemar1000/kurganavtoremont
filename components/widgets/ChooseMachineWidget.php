<?php

namespace app\components\widgets;

use Yii;
use yii\base\Widget;
use app\models\Machine;
use Html;

class ChooseMachineWidget extends Widget {

    /**
     * @inheritdoc
     */
    public function run() 
    {
        return $this->render('choose-machine', [
             'machines' => Machine::getMachineList(),
             'selection' => Yii::$app->request->cookies['machineId']->value,
         ]);
    }

}
