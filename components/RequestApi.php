<?php

namespace app\components;

use Yii;
use yii\httpclient\Client;

class RequestApi extends Model {

    public $apiHost;
    public $apiGeoHost;
    public $token;
    public $sid;
    public $uid;

    /**
     * RequestApi constructor.
     */
    public function __construct()
    {
        $this->apiHost = Yii::$app->params['apiHost'];
        $this->apiGeoHost = Yii::$app->params['apiGeoHost'];
        $this->token = Yii::$app->params['apiToken'];
        $this->auth();
    }

    /**
     * @param string $svc
     * @param string $params
     * @return bool
     */
    public function request($svc, $params)
    {
        $client = new Client();
        try {
            $response = $client->createRequest()
                    ->setMethod('post')
                    ->setUrl($this->apiHost)
                    ->setData([
                        'sid' => $this->sid,
                        'svc' => $svc,
                        'params' => $params
                    ])
                    ->send();
        } catch (\Exception $ex) {
            return false;
        }
        if ($response->isOk) {
            return $response->data;
        }
        return false;
    }

    /**
     * @param string $coords
     * @param string $flags
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function requestGeo($coords, $flags)
    {
        $client = new Client();
        $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl($this->apiGeoHost)
                ->setData([
                    'uid' => $this->uid,
                    'coords' => $coords,
                    'flags' => $flags
                ])
                ->send();
        if ($response->isOk) {
            return $response->data;
        }
        return false;
    }

    public function logout() {
        $svc = "core/logout";
        $params = '{}';
        $this->request($svc, $params);
    }

    /**
     * @return bool
     */
    public function auth()
    {
        $client = new Client();
        try {
            $response = $client->createRequest()
                    ->setMethod('post')
                    ->setUrl($this->apiHost)
                    ->setData([
                        'svc' => 'token/login',
                        'params' => '{"token": "' . $this->token . '"}'
                    ])
                    ->send();
        } catch (\Exception $ex) {
            return false;
        }
        if ($response->isOk) {
            $this->sid = $response->data['eid'];
            $this->gis_sid = $response->data['gis_sid'];
            $this->uid = $response->data['user']['id'];
            return true;
        }        
    }
}
