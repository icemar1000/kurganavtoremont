<?php
return [
    'viewer' => [
        'type' => 1,
        'ruleName' => 'userRole',
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userRole',
        'children' => [
            'viewer',
        ],
    ],
];
