<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\helpers\Wialon;
use app\models\Machine;
use app\models\Category;
use app\helpers\FormatterHelper;

class ApiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function actionGetSensors() 
    {
        $models = Machine::find()->where(['>', 'id', 1])->all();
        foreach($models as $model) {
            //echo $model->wialon_name . " - ";
            if($model->getSensors()) {
                echo "Success";
            } else {
                echo "Error";
            }
            echo "<br>";
        }
    }
    
    public function actionGetEngineHours() 
    {
        $models = Machine::find()->where(['>', 'id', 1])->all();
        foreach($models as $model) {
            //echo $model->wialon_name . " - ";
            if($model->getStats()) {
                echo "Успех";
            } else {
                echo "Ошибка";
            }
            echo "<br>";
        }
    }
}
