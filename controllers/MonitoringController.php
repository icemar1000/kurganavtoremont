<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\helpers\Wialon;
use app\models\Machine;
use app\models\Category;
use app\helpers\FormatterHelper;

class MonitoringController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {        
        $model = Machine::getCurrent();
        return $this->render('index', [
            'model' => $model
        ]);
    }
    
    public function actionTest()
    {        
        return $this->render('test');
    }
    
    public function actionInfo()
    {        
        $model = Machine::getCurrent();
        $categories = Category::find()->where(['machine_id' => $model->id, 'parent_id' => null]) ->all();
        
        return $this->render('info', [
            'model' => $model,
            'categories' => $categories,
        ]);
    }
    
    public function actionCondition()
    {        
        $model = Machine::getCurrent();
        
        return $this->render('condition', [
            'model' => $model
        ]);
    }
    
    public function actionRoute()
    {        
        $model = Machine::getCurrent();
        $post = Yii::$app->request->post();
        $interval = null;
        if ($post) {
            $interval = $post;
            if ($post['typeInterval'] == 'timeAgo') {
                $time = (isset($post['currentInterval'])) ? strtotime($post['choose-interval-from']) : time();
                $interval['from'] = $time - $post['periodLength'] * $post['periodType'];
                $interval['to'] = $time;
            } else {
                $interval['from'] = strtotime($post['choose-interval-from']);
                $interval['to'] = isset($post['choose-interval-to']) ? strtotime($post['choose-interval-to']) : time();
            }
            if ($interval['from'] > $interval['to']) {
                $_from = $interval['from'];
                $interval['from'] = $interval['to'];
                $interval['to'] = $_from;
            }
        }
        
        return $this->render('route', [
            'model' => $model,
            'interval' => $interval,
        ]);
    }
}
