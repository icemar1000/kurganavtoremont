<?php

use yii\db\Migration;

/**
 * Class m181106_092956_machine
 */
class m181106_092956_machine extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%machine}}', [
            'id' => $this->primaryKey(),
            'label' => $this->string()->notNull(),
            'wialon_id' => $this->integer(),
            'wialon_name' => $this->string(),
            'stats_update' => $this->integer(),
            'sensors' => $this->text(),
        ], $tableOptions);
        
        $this->insert('{{%machine}}', [
            'label' => 'Новая машина',
        ]);
        
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'label' => $this->string()->notNull(),
            'name' => $this->string(),
            'parent_id' => $this->integer(),
            'machine_id' => $this->integer()->notNull(),
            'complexFeature' => $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);   
        
        $this->createTable('{{%feature}}', [
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'label' => $this->string()->notNull(),
            'name' => $this->string(),
            'machine_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'is_calc' => $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);
        
        $this->createTable('{{%parameter}}', [
            'id' => $this->primaryKey(),
            'label' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);  
        
        $this->insert('{{%parameter}}', [
            'label' => 'Значение',
            'name' => 'default',
        ]);
        
        $this->insert('{{%parameter}}', [
            'label' => 'Марка/Модель',
            'name' => 'model',
        ]);
        
        $this->insert('{{%parameter}}', [
            'label' => 'Номер',
            'name' => 'modelNumber',
        ]);
        
        $this->createTable('{{%feature_value}}', [
            'id' => $this->primaryKey(),
            'machine_id' => $this->integer()->notNull(),
            'feature_id' => $this->integer()->notNull(),
            'parameter_id' => $this->integer()->notNull(),
            'value' => $this->string()->notNull(),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);  
        
        $this->addForeignKey('fk_user_to_machine', '{{%user}}', 'machine_id', '{{%machine}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_category_to_category', '{{%category}}', 'parent_id', '{{%category}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_category_to_machine', '{{%category}}', 'machine_id', '{{%machine}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_feature_to_category', '{{%feature}}', 'category_id', '{{%category}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_feature_value_to_machine', '{{%feature_value}}', 'machine_id', '{{%machine}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_feature_value_to_feature', '{{%feature_value}}', 'feature_id', '{{%feature}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_feature_value_to_parameter', '{{%feature_value}}', 'parameter_id', '{{%parameter}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tables = [
            '{{%machine}}',
            '{{%category}}',
            '{{%feature}}',
            '{{%parameter}}',
            '{{%feature_value}}'
        ];

        foreach ($tables as $table) {
            $this->dropTable($table);
        }
    }
}
