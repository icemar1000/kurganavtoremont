<?php

use yii\db\Migration;

/**
 * Class m190128_064013_statistics
 */
class m190128_064013_statistics extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%statistics}}', [
            'id' => $this->primaryKey(),
            'machine_id' => $this->integer()->notNull(),
            'time' => $this->integer(),
            'x' => $this->float(),
            'y' => $this->float(),
            'speed' => $this->integer(),            
            'engine_speed' => $this->integer()
        ], $tableOptions);
        
        $this->addForeignKey('fk_stat_to_machine', '{{%statistics}}', 'machine_id', '{{%machine}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('statistics');
    }

}
