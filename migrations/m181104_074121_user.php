<?php

use yii\db\Migration;
use app\models\User;

class m181104_074121_user extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(User::AUTH_KEY_LENGTH)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(User::STATUS_HOLD),
            'role' => $this->string()->notNull()->defaultValue(User::ROLE_VIEWER),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'machine_id' => $this->integer(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $tables = [
            '{{%user}}'
        ];

        foreach ($tables as $table) {
            $this->dropTable($table);
        }
    }
}
